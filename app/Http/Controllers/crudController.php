<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\crud;
use Session;
class crudController extends Controller
{
    public function display()
    {
        // $read = crud::all();
        $read = crud::paginate(5); // for showing by number
        // $read = crud::simplePaginate(5); // for showing by next and previous button
        return view('show', compact('read'));
    }
    public function insert()
    {
        return view('add');
    }
    public function save(Request $req)
    {
        $validation = [
            'name' => 'required | max:20',
            'email' => 'required | email',
            'phone' => 'required | max:11 | min:11'
        ];
        $customMessage = [
            'name.required' => 'Enter Your Name',
            'name.max' => 'Character can not be more than 20',
            'email.required' => 'Enter Your Email',
            'email.email' => 'Email must be a Valid Email',
            'phone.required' => 'Enter Your Number',
            'phone.max' => 'Your Number must be 11 Digits',
            'phone.min' => 'Your Number must be 11 Digits',
        ];
        $this->validate($req, $validation, $customMessage);
        $c = new crud();
        $c->name = $req->name;
        $c->email = $req->email;
        $c->phone = $req->phone;
        $c->save();
        Session::flash('message', 'Data is added Successfully');
        return redirect('/');
    }

    public function edit($id=null)
    {
        $editData = crud::find($id); 
        return view('edit', compact('editData'));
    }
    public function update(Request $req, $id)
    {
        $validation = [
            'name' => 'required | max:20',
            'email' => 'required | email',
            'phone' => 'required | max:11 | min:11'
        ];
        $customMessage = [
            'name.required' => 'Enter Your Name',
            'name.max' => 'Character can not be more than 20',
            'email.required' => 'Enter Your Email',
            'email.email' => 'Email must be a Valid Email',
            'phone.required' => 'Enter Your Number',
            'phone.max' => 'Your Number must be 11 Digits',
            'phone.min' => 'Your Number must be 11 Digits',
        ];
        $this->validate($req, $validation, $customMessage);
        $c = crud::find($id);
        $c->name = $req->name;
        $c->email = $req->email;
        $c->phone = $req->phone;
        $c->save();
        Session::flash('message', 'Data is updated Successfully');
        return redirect('/');
    }

    public function delete($id=null)
    {
        $deleteData = crud::find($id);
        $deleteData->delete();
        Session::flash('message', 'Data has been deleted Successfully');
        return redirect('/');
    }
}
