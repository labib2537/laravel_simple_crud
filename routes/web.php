<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\crudController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/',[crudController::class, 'display']);
Route::get('/add',[crudController::class, 'insert']);
Route::post('/store',[crudController::class, 'save']);
Route::get('/edit/{id}',[crudController::class, 'edit']);
Route::post('/update/{id}',[crudController::class, 'update']);
Route::get('/delete/{id}',[crudController::class, 'delete']);