
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
        <a href="{{'/'}}" class="btn btn-primary my-3">Show All Data</a>
        <form class="row g-3" action="{{'store'}}" method="post">
          @csrf
  <div class="col-12">
    <label for="name" class="form-label">Name</label>
    <input type="text" name="name" class="form-control" id="name">
    @error('name')
    <span class="text-danger">{{$message}}</span>
    @enderror
  </div>
  <div class="col-12">
    <label for="email" class="form-label">Email</label>
    <input type="text" name="email" class="form-control" id="email">
    @error('email')
    <span class="text-danger">{{$message}}</span>
    @enderror
  </div>
  <div class="col-12">
    <label for="phone" class="form-label">Phone</label>
    <input type="text" name="phone" class="form-control" id="phone">
    @error('phone')
    <span class="text-danger">{{$message}}</span>
    @enderror
  </div>

  <div class="col-12">
    <button type="submit" class="btn btn-primary">Save</button>
  </div>
</form>
    
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
  </body>
</html>